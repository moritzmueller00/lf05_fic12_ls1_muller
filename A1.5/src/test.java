
public class test {

	public static void main(String[] args) {
		System.out.print("Hallo ich bin Moritz.");
		System.out.println(" Ich mag alte Autos.");
		System.out.println("Ich habe einen 3000GT VR4");
		
		// "print" gibt den text wieder
		// "printls" gibt den text wieder und beendet die zeile

		System.out.println("    *    ");
		System.out.println("   ***   ");
		System.out.println("  *****  ");
		System.out.println(" ******* ");
		System.out.println("   ***   ");
		System.out.println("   ***   ");
		
		System.out.printf("|%.2f|%n",22.4234234);
		System.out.printf("|%.2f|%n",111.2222);
		System.out.printf("|%.2f|%n",4.0);
		System.out.printf("|%.2f|%n",1000000.551);
		System.out.printf("|%.2f|%n",97.34);
		
	}

}
