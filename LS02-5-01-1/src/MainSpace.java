
public class MainSpace {
	private String RaumschiffName;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildInProzent;
	private int huelleInProzent;
	
	
	public MainSpace() {
		System.out.println("Raumschiff-Objekt: ohne Parameter");
		
	}
	
	public Raumschiff(String marke , String farbe) {
		
		System.out.println("Auto-Objekt: mit Parameter");
		
		this.marke = marke;
		this.farbe = farbe;
		
	}

	public void setMarke(String marke) {
		this.marke = marke;
		
	}
	
	public String getMarke() {
		return this.marke;
		
	}
	public void setFarbe(String farbe) {
		this.farbe = farbe;
		
	}
	
	public String getFarbe() {
		return this.farbe;
		
	}
	// Weitere Methoden
	public void fahren(int strecke) {
		
		System.out.println("3 2 1 GOOOOOOOO");
		
	}
	
	public void tanken(int lieter) {
		System.out.println("Sper+ 10 Euro!");
		
	}
}
}
