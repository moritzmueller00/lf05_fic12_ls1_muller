import java.util.Scanner;

public class Fahrkarten {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;

		do {
			// Bestellung erfassen
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			// Eingezahlen Betrag erfassen
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrkarte drucken
			fahrkartenAusgeben();
			// R�ckgeld geben
			rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);

			warte(3450); // delay, bevor eine neue Bestellung aufgenommen wird in ms \\
		} while (true);

	}

	private static void warte(int milisecond) {
		try {
			Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	// gibt den Gesamtpreis der Tickets zur�ck
	private static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0;
		int tickets;
		boolean ticketInputCheck = true;
		int ticketChoice = 0;

		// Ticketauswahl

		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus \n"
				+ " Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" + " Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
				+ " Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n\n");

		do { // eingabe + �berpr�fung ob valid
			ticketChoice = tastatur.nextInt();

			System.out.println("Ihre Wahl: " + ticketChoice);

			// checks validity
			if ((ticketChoice > 0) && (ticketChoice < 4))
				ticketInputCheck = false;
			else
				System.out.println(" >>falsche Eingabe<<");

		} while (ticketInputCheck);

		// Eingabe Anzahl der Tickets
		System.out.print("Anzahl der Tickets: ");
		tickets = tastatur.nextInt();

		if ((tickets > 10) || (tickets < 1)) {
			// ung�ltiger Eingabebereich (<1 / >10 Tickets)
			tickets = 1;
			System.out.println("Ung�ltige Eingabe! Bestellung wird f�r (1) Ticket fortgef�hrt!");
		}

		// berechnung des Ticket Preises, je nach Auswahl
		switch (ticketChoice) {
		case 1:
			zuZahlenderBetrag = 2.90 * tickets;
			break;
		case 2:
			zuZahlenderBetrag = 8.60 * tickets;
			break;
		case 3:
			zuZahlenderBetrag = 23.50 * tickets;
			break;
		}

		return zuZahlenderBetrag;
	}

	// gibt den eingezahlten Betrag zur�ck
	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu Zahlender Betrag %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag;
	}

	// animierte Fahrscheinausgabe
	private static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");

		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n");

	}

	// Textausgabe, welche M�nzen als R�ckgeld ausgegeben werden
	private static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f", r�ckgabebetrag);
			System.out.println(" EURO \nwird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "Cent");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "Cent");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "Cent");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "Cent");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.\n" + "______________________________________\n\n");
	}

}
